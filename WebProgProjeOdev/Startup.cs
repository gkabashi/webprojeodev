﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebProgProjeOdev.Startup))]
namespace WebProgProjeOdev
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
